#!/bin/bash

set -e

export INSTALL="$(pwd)/install"
export SRC="$(pwd)/src"
export DEB_VERSION=0.2.5

mkdir -p $INSTALL
rm -rf $INSTALL/*

cd $SRC
if [ -n "$CIRCLECI" ]; then
  sudo apt install -y python3-tk python3-pydicom
  sudo -H pip3 install pyinstaller setuptools
fi

#virtualenv -p /usr/bin/python3.6 venv
#source venv/bin/activate

#pip install PyQt5 future pydicom setuptools

fpm --verbose -s python -t deb -n bids-manager -v $DEB_VERSION \
   --description="BIDS manager" \
   -d "anywave = 0.20191114" \
   -d python3-future \
   -d python3-tk \
   -d python3-qtpy \
   -d python3-pydicom \
   --deb-no-default-config-files \
   --python-bin /usr/bin/python3 \
   --python-install-bin /usr/local/bin/ \
   -p ../bids-manager_${DEB_VERSION}_amd64.deb \
   setup.py 
